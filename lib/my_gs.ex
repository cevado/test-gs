defmodule MyGS do
  def start_link(module, initial) do
    pid = spawn_link(__MODULE__, :init, [module, initial, self()])

    receive do
      :ok -> {:ok, pid}
      error -> error
    end
  end

  def init(module, initial, starter) do
    case module.init(initial) do
      {:ok, state} ->
        send(starter, :ok)
        loop(module, state)

      error ->
        send(starter, error)
    end
  end

  def cast(pid, message) do
    send(pid, {:cast, message})

    {:ok, message}
  end

  def call(pid, message) do
    send(pid, {:call, self(), message})

    receive do
      response -> response
    end
  end

  defp loop(module, state) do
    receive do
      {:call, caller, message} ->
        case module.handle_call(message, caller, state) do
          {:reply, response, new_state} ->
            send(caller, response)
            loop(module, new_state)

          error ->
            raise error
        end

      {:cast, message} ->
        case module.handle_cast(message, state) do
          {:noreply, new_state} -> loop(module, new_state)
          error -> raise error
        end

      any ->
        case module.handle_info(any, state) do
          {:noreply, new_state} -> loop(module, new_state)
          error -> raise error
        end
    end
  end
end
